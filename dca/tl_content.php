<?php

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  cgo IT, 2014
 * @author     Carsten G�tzinger (info@cgo-it.de)
 * @package    dynamic_columns
 * @license    GNU/LGPL
 * @filesource
 */

/**
 * palettes
 */
$GLOBALS['TL_DCA']['tl_content']['palettes']['__selector__'][] = 'dyncol_expert_options';

$GLOBALS['TL_DCA']['tl_content']['palettes']['dynamic_columns'] = '{type_legend},type,headline;{dyncol_legend},dyncol_columns,dyncol_max_width,dyncol_classname_screen,dyncol_classname_print,dyncol_expert_options;{text_legend},text;{image_legend},addImage;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space;{invisible_legend:hide},invisible,start,stop';

/**
 * Add subpalettes to tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['dyncol_expert_options']  = 'dyncol_extraHeight,dyncol_minSplitHeight,dyncol_minHeight,dyncol_readOnText';

/**
 * fields
 */
$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_columns'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_columns'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_max_width'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_max_width'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_classname_screen'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_classname_screen'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_classname_print'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_classname_print'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_expert_options'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_expert_options'],
		'default'                 => '',
		'exclude'                 => true,
		'inputType'               => 'checkbox',
		'eval'                    => array('tl_class'=>'w50', 'submitOnChange'=>true)
);

$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_extraHeight'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_extraHeight'],
		'exclude'                 => true,
		'inputType'               => 'text',
		'eval'                    => array('tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_minSplitHeight'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_minSplitHeight'],
		'exclude'                 => true,
		'inputType'               => 'text',
		'eval'                    => array('tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_minHeight'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_minHeight'],
		'exclude'                 => true,
		'inputType'               => 'text',
		'eval'                    => array('tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_content']['fields']['dyncol_readOnText'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_content']['dyncol_readOnText'],
		'exclude'                 => true,
		'inputType'               => 'text',
		'eval'                    => array('tl_class'=>'w50')
);

