<?php

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  cgo IT, 2014
 * @author     Carsten G�tzinger (info@cgo-it.de)
 * @package    dynamic_columns
 * @license    GNU/LGPL
 * @filesource
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace cgoIT\dynamic_columns;


/**
 * Class ContentDynamicColumn
 *
 * Front end content element "dynamic columns".
 */
class ContentDynamicColumn extends \ContentText
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_dynamic_columns';


	/**
	 * Generate the content element
	 */
	protected function compile() {
		global $objPage;
		
		$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/dynamic_columns/public/js/multicolumn.js|static';
		$GLOBALS['TL_CSS'][] = 'system/modules/dynamic_columns/public/css/multicolumn.css||static';
		
		$strName = substr(md5("columnized-$this->id"), 0, 12);
		
		// JS-Datei erstellen
		$strJsName = 'assets/js/' . $strName . '.js';
		$objJsFile = new \File($strJsName);
		$objJsFile->truncate();
		$objJsFile->append($this->getJsContent());
		$objJsFile->close();
		$GLOBALS['TL_JAVASCRIPT'][] = $strJsName.'|static';
		
		// CSS-Datei erstellen
		$cssContent = $this->getCssContent();
		if ($cssContent) {
			$strCssName = 'assets/css/' . $strName . '.css';
			$objCssFile = new \File($strCssName);
			$objCssFile->truncate();
			$objCssFile->append($this->getCssContent());
			$objCssFile->close();
			$GLOBALS['TL_CSS'][] = $strCssName.'||static';
		}
		
		parent::compile();
	}
	
	private function getJsContent() {
		global $objPage;
		
		$content = "
    var multiColumnSettings=new MultiColumnSettings;";
		if(strlen($this->dyncol_classname_screen) > 0) {
			$content .= "
			multiColumnSettings.classNameScreen='$this->dyncol_classname_screen';";
		}
		if(strlen($this->dyncol_classname_print) > 0) {
			$content .= "
			multiColumnSettings.classNamePrint='$this->dyncol_classname_print';";
		}
		if(intval($this->dyncol_columns) > 0) {
			$content .= "
 	multiColumnSettings.numberOfColumns=".intval($this->dyncol_columns).";";
		}
		if(intval($this->dyncol_extraHeight) > 0) {
			$content .= "
 	multiColumnSettings.extraHeight=".intval($this->dyncol_extraHeight).";";
		}
		if(intval($this->dyncol_minSplitHeight) > 0) {
			$content .= "
 	multiColumnSettings.minSplitHeight=".intval($this->dyncol_minSplitHeight).";";
		}
		if(intval($this->dyncol_minHeight) > 0) {
			$content .= "
 	multiColumnSettings.minHeight=".intval($this->dyncol_minHeight).";";
		}
		if(strlen($this->dyncol_readOnText) > 0) {
			$content .= "
			multiColumnSettings.readOnText='$this->dyncol_readOnText';";
		}
		if ($objPage->hasJQuery) {
			$content .= "
			$(document).ready(function() {
				new MultiColumn(document.getElementById(\"columnized-$this->id\"), multiColumnSettings);
			});";
		} else if ($objPage->hasMooTools) {
			$content .= "
			(function($) {
				window.addEvent('domready', function() {
				new MultiColumn(document.getElementById(\"columnized-$this->id\"), multiColumnSettings);
			});
			})(document.id);";
		}
		
		return $content;
	}
	
	private function getCssContent() {
		if (intval($this->dyncol_max_width) == 0) return false;
		$content = "
		#columnized-$this->id div {
			width: ".intval($this->dyncol_max_width)."px;
		}";
		return $content;
	}
}
