<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package Ce_popup
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'cgoIT',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'cgoIT\dynamic_columns\ContentDynamicColumn' => 'system/modules/dynamic_columns/classes/ContentDynamicColumn.php',
));

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'ce_dynamic_columns'  => 'system/modules/dynamic_columns/templates',
));
