-- **********************************************************
-- *                                                        *
-- * IMPORTANT NOTE                                         *
-- *                                                        *
-- * Do not import this file manually but use the TYPOlight *
-- * install tool to create and maintain database tables!   *
-- *                                                        *
-- **********************************************************


-- 
-- Table `tl_content`
-- 
CREATE TABLE `tl_content` (
  `dyncol_columns` char(2) NOT NULL default '',
  `dyncol_max_width` char(10) NOT NULL default '',
  `dyncol_classname_screen` char(255) NOT NULL default '',
  `dyncol_classname_print` char(255) NOT NULL default '',
  `dyncol_expert_options` char(1) NOT NULL default '',
  `dyncol_extraHeight` char(10) NOT NULL default '',
  `dyncol_minSplitHeight` char(10) NOT NULL default '',
  `dyncol_minHeight` char(10) NOT NULL default '',
  `dyncol_readOnText` char(255) NOT NULL default '',
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
