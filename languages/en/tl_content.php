<?php 

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  cgo IT, 2014
 * @author     Carsten G�tzinger (info@cgo-it.de)
 * @package    dynamic_columns
 * @license    GNU/LGPL
 * @filesource
 */


/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['tl_content']['dyncol_legend'] =  'Dynamic Columns';
$GLOBALS['TL_LANG']['tl_content']['dyncol_columns'] =  array( 'Anzahl Spalten' , 'Eine feste Anzahl an Spalten verwenden (=Breite wird automatisch berechnet).' );
$GLOBALS['TL_LANG']['tl_content']['dyncol_max_width'] =  array( 'Max. Breite einer Spalte' , 'Max. Breite einer Spalte in Pixeln (=Anzahl Spalten wird automatisch berechnet).' );
$GLOBALS['TL_LANG']['tl_content']['dyncol_classname_screen'] =  array( 'CSS-Klasse der Spalten-DIV (Screen)' , 'CSS-Klasse, die das DIV der einzelnen Spalten bei einer Anzeige am Bildschirm erh�lt.' );
$GLOBALS['TL_LANG']['tl_content']['dyncol_classname_print'] =  array( 'CSS-Klasse der Spalten-DIV (Print)' , 'CSS-Klasse, die das DIV der einzelnen Spalten beim Ausdruck erh�lt.' );
$GLOBALS['TL_LANG']['tl_content']['dyncol_expert_options'] =  array( 'Erweiterte Einstellungen' , 'Weitere Einstellungen für Spaltendarstellung festlegen.' );
$GLOBALS['TL_LANG']['tl_content']['dyncol_extraHeight'] =  array( 'Zusätzliche Höhe in Pixel' , 'Add extra height to a column. Increase this if the last column \'sticks out\' too much.' );
$GLOBALS['TL_LANG']['tl_content']['dyncol_minSplitHeight'] =  array( 'Minimale Spaltenhöhe für Umbruch in Pixel' , 'If the base column is smaller than minSplitHeight, the column does not split.' );
$GLOBALS['TL_LANG']['tl_content']['dyncol_minHeight'] =  array( 'Minimale Spaltenhöhe' , 'Minimum height of a column' );
$GLOBALS['TL_LANG']['tl_content']['dyncol_readOnText'] =  array( 'Text \'Weiterlesen\' Link' , 'Add the "read on" notice a the bottom of each column' );
